// pages/user/user.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    location:""
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getlocation:function(){
      wx.showLoading({
        title: '定位中...',
        success:()=>{
          wx.getLocation({
            isHighAccuracy:true,
            success:(res)=>{
              //console.log(res);
              //内置地图打开当前位置
              // wx.openLocation({
              //   latitude: res.latitude,
              //   longitude: res.longitude,
              // })

              wx.request({
                url: 'http://api.map.baidu.com/geocoder',
                data:{
                  location:res.latitude+","+res.longitude,
                  output:"json",
                  ak:"百度地图密匙",
                  v:"2.0"
                },
                success:(res)=>{

                  wx.hideLoading();
                  console.log(res.data.result.formatted_address);
                  this.setData({
                    location:res.data.result.formatted_address
                  })
                
                
                }
              })


            }
          })
        }
      })



    }
  }
})
